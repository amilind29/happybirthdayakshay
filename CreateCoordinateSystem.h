/*
this file provides functions required for creating coodinate system with given boundry

if needed this system can be moved relative to the origin
*/

/*
Object creates the movable coodiate system which can be manipulates to move the shapes drawn with it
*/
#ifndef COORDINATE_SYSTEM
#define COORDINATE_SYSTEM

#define DEFAULT_SCALE 0.8f
typedef struct
{
    /*
        xLimit - whole number division for the coordinate system along x axis
        xLimit -  whole number division for the coordinate system along Y axis
        originX - actual origin of the coodinate system in the (-1 to +1 range required by library) along x axis
        originY - actual origin of the coodinate system in the (-1 to +1 range required by library) along Y axis
    */
    int xLimit; // divides 0 to 1 range in x equal parts
    int yLimit; // divides 0 to 1 range in Y equal parts
    float originX;
    float originY;
    float scale; // it will decide the relative size of shape when actually its drawn in the coodinate system
} CoordinateSystem;

typedef struct
{
    float xPosition;
    float yPosition;
} Point;

typedef struct
{
    int xPosition;
    int yPosition;
} RelativePoint;

void createCoordiNateSystemWithDefaultScale(int xLimit, int yLimit, float originX, float originY, CoordinateSystem *pCoordinateSystem)
{
    // function declarations
    void createCoordiNateSystemWithScale(int xLimit, int yLimit, float originX, float originY, float scale, CoordinateSystem *pCoordinateSystem);

    createCoordiNateSystemWithScale(xLimit, yLimit, originX, originY, DEFAULT_SCALE, pCoordinateSystem);
}

void createCoordiNateSystemWithScale(int xLimit, int yLimit, float originX, float originY, float scale, CoordinateSystem *pCoordinateSystem)
{
    pCoordinateSystem->xLimit = xLimit;
    pCoordinateSystem->yLimit = yLimit;
    pCoordinateSystem->originX = originX;
    pCoordinateSystem->originY = originY;
    pCoordinateSystem->scale = scale;
}

Point createRectanglePoint(int x, int y, CoordinateSystem coordinateSystem)
{
    Point point;
    point.xPosition = (1.0f / (float)coordinateSystem.xLimit) * ((float)x) * coordinateSystem.scale;
    point.yPosition = (1.0f / (float)coordinateSystem.yLimit) * ((float)y) * coordinateSystem.scale;
    return point;
}

RelativePoint createRelativePoint(int x, int y)
{
    RelativePoint point;
    point.xPosition = x;
    point.yPosition = y;
    return point;
}

#endif