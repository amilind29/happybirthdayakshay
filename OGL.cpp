// header files
#include <GL/freeglut.h>
#include "CreateCoordinateSystem.h"
#include "ColorSystem.h"

// for happy birthday text
#define SCALE 0.7
#define X_RANGE 20
#define Y_RANGE 15
#define origin_X 0.0f
#define origin_Y 0.0f

// global variable declarations
bool bIsFullScreen = false;

// entry-point function
int main(int argc, char *argv[])
{
    // function declarations
    void initialize(void);
    void resize(int, int);
    void display(void);
    void keyboard(unsigned char, int, int);
    void mouse(int, int, int, int);
    void uninitialize(void);

    // code
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

    glutInitWindowSize(800, 600);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("My First RTR5 Program : Milind Arun Auti");

    initialize();

    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutCloseFunc(uninitialize);

    glutMainLoop();
    return (0);
}

void initialize(void)
{
    // code
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
    // code
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
    // function declarations
    void drawPolygon();
    // code
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    drawPolygon(); // background for the demo
    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
    // code
    switch (key)
    {
    case 27:
        glutLeaveMainLoop();
        break;
    case 'F':
    case 'f':
        if (bIsFullScreen == false)
        {
            glutFullScreen();
            bIsFullScreen = true;
        }
        else
        {
            glutLeaveFullScreen();
            bIsFullScreen = false;
        }
        break;
    default:
        break;
    }
}

void mouse(int button, int state, int x, int y)
{
    // code
    switch (button)
    {
    case GLUT_RIGHT_BUTTON:
        glutLeaveMainLoop();
        break;
    default:
        break;
    }
}

void uninitialize(void)
{
    // code
}

void drawPolygon()
{
    glBegin(GL_POLYGON);

    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, 0.0f);
    glColor3f(1.0f, 0.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, 0.0f);
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3f(-1.0f, -1.0f, 0.0f);
    glColor3f(0.0f, 0.0f, 0.0f);
    glVertex3f(-1.0f, 1.0f, 0.0f);

    glEnd();

    // function declarations
    void createHappyBirthdayString();

    // code
    createHappyBirthdayString();
}

void drawRectangle(Point p1, Point p2, Color rectangleColor)
{
    // variable declarations
    void drawShaddow(Point p1, Point p2);

    // code
    glBegin(GL_POLYGON);
    glColor3f(rectangleColor.red, rectangleColor.green, rectangleColor.blue);
    glVertex3f(p1.xPosition, p1.yPosition, 0.0f);
    glVertex3f(p1.xPosition, p2.yPosition, 0.0f);
    glVertex3f(p2.xPosition, p2.yPosition, 0.0f);
    glVertex3f(p2.xPosition, p1.yPosition, 0.0f);
    glEnd();
    drawShaddow(p1, p2);
}

void drawShaddow(Point p1, Point p2)
{

    // code
    glBegin(GL_POLYGON);
    glColor3f(GREY.red, GREY.green, GREY.blue);
    glVertex3f(p1.xPosition, 0.0f, p1.yPosition);
    glVertex3f(p1.xPosition, 0.0f, p2.yPosition);
    glVertex3f(p2.xPosition, 0.0f, p2.yPosition);
    glVertex3f(p2.xPosition, 0.0f, p1.yPosition);
    glEnd();
}

// h
// letter position start from 0
void createH(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord, int letterPositionInWord)
{
    // variable declarations
    // 1st rect
    Point p1, p2;

    // 2nd rect
    Point p3, p4;

    // 3rd rect
    Point p5, p6;

    // code
    startCoordinateForWord.xPosition = startCoordinateForWord.xPosition + (letterPositionInWord * 4);
    // rect 1
    p1 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition, coordinateSystem);
    p2 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p1, p2, RED);
    // rect 2
    p3 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition, coordinateSystem);
    p4 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p3, p4, RED);
    // rect 3
    p5 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 2, coordinateSystem);
    p6 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 3, coordinateSystem);
    drawRectangle(p5, p6, RED);
}

// a
void createA(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord, int letterPositionInWord)
{
    // variable declarations
    // 1st rect
    Point p1, p2;

    // 2nd rect
    Point p3, p4;

    // 3rd rect
    Point p5, p6;

    // 4th rect
    Point p7, p8;

    // code
    // code
    startCoordinateForWord.xPosition = startCoordinateForWord.xPosition + (letterPositionInWord * 4);
    // rect 1
    p1 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition, coordinateSystem);
    p2 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p1, p2, RED);
    // rect 2
    p3 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition, coordinateSystem);
    p4 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p3, p4, RED);
    // rect 3
    p5 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 3, coordinateSystem);
    p6 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p5, p6, RED);

    // rect 4
    p7 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 1, coordinateSystem);
    p8 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 2, coordinateSystem);
    drawRectangle(p7, p8, RED);
}

// p
void createP(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord, int letterPositionInWord)
{
    // variable declarations
    // 1st rect
    Point p1, p2;

    // 2nd rect
    Point p3, p4;

    // 3rd rect
    Point p5, p6;

    // 4th rect
    Point p7, p8;

    // code
    // code
    startCoordinateForWord.xPosition = startCoordinateForWord.xPosition + (letterPositionInWord * 4);
    // rect 1
    p1 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition, coordinateSystem);
    p2 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p1, p2, RED);
    // rect 2
    p3 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 1, coordinateSystem);
    p4 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p3, p4, RED);
    // rect 3
    p5 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 3, coordinateSystem);
    p6 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p5, p6, RED);

    // rect 4
    p7 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 1, coordinateSystem);
    p8 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 2, coordinateSystem);
    drawRectangle(p7, p8, RED);
}
// y
void createY(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord, int letterPositionInWord)
{
    // variable declarations
    // 1st rect
    Point p1, p2;

    // 2nd rect
    Point p3, p4;

    // 3rd rect
    Point p5, p6;

    // code
    // code
    startCoordinateForWord.xPosition = startCoordinateForWord.xPosition + (letterPositionInWord * 4);
    // rect 1
    p1 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition + 4, coordinateSystem);
    p2 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 2, coordinateSystem);
    drawRectangle(p1, p2, RED);
    // rect 2
    p3 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 2, coordinateSystem);
    p4 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 0, coordinateSystem);
    drawRectangle(p3, p4, RED);
    // rect 3
    p5 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 4, coordinateSystem);
    p6 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 2, coordinateSystem);
    drawRectangle(p5, p6, RED);
}
// b
void createB(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord, int letterPositionInWord)
{
    // variable declarations
    // 1st rect
    Point p1, p2;

    // 2nd rect
    Point p3, p4;

    // 3rd rect
    Point p5, p6;

    // 4th rect
    Point p7, p8;

    // code
    // code
    startCoordinateForWord.xPosition = startCoordinateForWord.xPosition + (letterPositionInWord * 4);
    // rect 1
    p1 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition, coordinateSystem);
    p2 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p1, p2, RED);
    // rect 2
    p3 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 1, coordinateSystem);
    p4 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 3, coordinateSystem);
    drawRectangle(p3, p4, RED);
    // rect 3
    p5 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 0, coordinateSystem);
    p6 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 1, coordinateSystem);
    drawRectangle(p5, p6, RED);

    // rect 4
    p7 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 2, coordinateSystem);
    p8 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 3, coordinateSystem);
    drawRectangle(p7, p8, RED);
}
// i
void createI(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord, int letterPositionInWord)
{
    // variable declarations
    // 1st rect
    Point p1, p2;

    // 2nd rect
    Point p3, p4;

    // 3rd rect
    Point p5, p6;

    // code
    // code
    startCoordinateForWord.xPosition = startCoordinateForWord.xPosition + (letterPositionInWord * 4);
    // rect 1
    p1 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition, coordinateSystem);
    p2 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 1, coordinateSystem);
    drawRectangle(p1, p2, RED);
    // rect 2
    p3 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 1, coordinateSystem);
    p4 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 3, coordinateSystem);
    drawRectangle(p3, p4, RED);
    // rect 3
    p5 = createRectanglePoint(startCoordinateForWord.xPosition + 0, startCoordinateForWord.yPosition + 3, coordinateSystem);
    p6 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p5, p6, RED);
}
// r
void createR(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord, int letterPositionInWord)
{
    // variable declarations
    // 1st rect
    Point p1, p2;

    // 2nd rect
    Point p3, p4;

    // 3rd rect
    Point p5, p6;

    // 4th rect
    Point p7, p8;

    // code
    // code
    startCoordinateForWord.xPosition = startCoordinateForWord.xPosition + (letterPositionInWord * 4);
    // rect 1
    p1 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition, coordinateSystem);
    p2 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p1, p2, RED);
    // rect 2
    p3 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 2, coordinateSystem);
    p4 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p3, p4, RED);
    // rect 3
    p5 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 3, coordinateSystem);
    p6 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p5, p6, RED);

    // rect 4
    p7 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 1, coordinateSystem);
    p8 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 2, coordinateSystem);
    drawRectangle(p7, p8, RED);

    // rect 5
    p7 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 0, coordinateSystem);
    p8 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 1, coordinateSystem);
    drawRectangle(p7, p8, RED);
}
// t
void createT(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord, int letterPositionInWord)
{
    // variable declarations
    // 1st rect
    Point p1, p2;

    // 2nd rect
    Point p3, p4;

    // code
    // code
    startCoordinateForWord.xPosition = startCoordinateForWord.xPosition + (letterPositionInWord * 4);
    // rect 1
    p1 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition + 3, coordinateSystem);
    p2 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p1, p2, RED);
    // rect 2
    p3 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition, coordinateSystem);
    p4 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 3, coordinateSystem);
    drawRectangle(p3, p4, RED);
}

// d
void createD(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord, int letterPositionInWord)
{
    // variable declarations
    // 1st rect
    Point p1, p2;

    // 2nd rect
    Point p3, p4;

    // 3rd rect
    Point p5, p6;

    // 4th rect
    Point p7, p8;

    // code
    // code
    startCoordinateForWord.xPosition = startCoordinateForWord.xPosition + (letterPositionInWord * 4);
    // rect 1
    p1 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition, coordinateSystem);
    p2 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p1, p2, RED);
    // rect 2
    p3 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition, coordinateSystem);
    p4 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 3, coordinateSystem);
    drawRectangle(p3, p4, RED);
    // rect 3
    p5 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 2, coordinateSystem);
    p6 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 3, coordinateSystem);
    drawRectangle(p5, p6, RED);

    // rect 4
    p7 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 0, coordinateSystem);
    p8 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 1, coordinateSystem);
    drawRectangle(p7, p8, RED);
}
void createK(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord, int letterPositionInWord)
{
    // variable declarations
    // 1st rect
    Point p1, p2;

    // 2nd rect
    Point p3, p4;

    // 3rd rect
    Point p5, p6;

    // 4th rect
    Point p7, p8;

    // code
    // code
    startCoordinateForWord.xPosition = startCoordinateForWord.xPosition + (letterPositionInWord * 4);
    // rect 1
    p1 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition, coordinateSystem);
    p2 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p1, p2, RED);
    // rect 2
    p3 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 0, coordinateSystem);
    p4 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 2, coordinateSystem);
    drawRectangle(p3, p4, RED);
    // rect 3
    p5 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 3, coordinateSystem);
    p6 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p5, p6, RED);

    // rect 4
    p7 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 2, coordinateSystem);
    p8 = createRectanglePoint(startCoordinateForWord.xPosition + 2, startCoordinateForWord.yPosition + 3, coordinateSystem);
    drawRectangle(p7, p8, RED);
}

void createS(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord, int letterPositionInWord)
{
    // variable declarations
    // 1st rect
    Point p1, p2;

    // 2nd rect
    Point p3, p4;

    // 3rd rect
    Point p5, p6;

    // 4th rect
    Point p7, p8;

    // code
    // code
    startCoordinateForWord.xPosition = startCoordinateForWord.xPosition + (letterPositionInWord * 4);
    // rect 1
    p1 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition, coordinateSystem);
    p2 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 1, coordinateSystem);
    drawRectangle(p1, p2, RED);
    // rect 2
    p3 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition + 3, coordinateSystem);
    p4 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 4, coordinateSystem);
    drawRectangle(p3, p4, RED);
    // rect 3
    p5 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 1, coordinateSystem);
    p6 = createRectanglePoint(startCoordinateForWord.xPosition + 3, startCoordinateForWord.yPosition + 2, coordinateSystem);
    drawRectangle(p5, p6, RED);

    // rect 4
    p7 = createRectanglePoint(startCoordinateForWord.xPosition, startCoordinateForWord.yPosition + 2, coordinateSystem);
    p8 = createRectanglePoint(startCoordinateForWord.xPosition + 1, startCoordinateForWord.yPosition + 3, coordinateSystem);
    drawRectangle(p7, p8, RED);
}

void writeHappy(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord)
{
    createH(coordinateSystem, startCoordinateForWord, 0);
    createA(coordinateSystem, startCoordinateForWord, 1);
    createP(coordinateSystem, startCoordinateForWord, 2);
    createP(coordinateSystem, startCoordinateForWord, 3);
    createY(coordinateSystem, startCoordinateForWord, 4);
}

void writeBirthday(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord)
{
    createB(coordinateSystem, startCoordinateForWord, 0);
    createI(coordinateSystem, startCoordinateForWord, 1);
    createR(coordinateSystem, startCoordinateForWord, 2);
    createT(coordinateSystem, startCoordinateForWord, 3);
    createH(coordinateSystem, startCoordinateForWord, 4);
    createD(coordinateSystem, startCoordinateForWord, 5);
    createA(coordinateSystem, startCoordinateForWord, 6);
    createY(coordinateSystem, startCoordinateForWord, 7);
}

void writeAkshay(CoordinateSystem coordinateSystem, RelativePoint startCoordinateForWord)
{
    createA(coordinateSystem, startCoordinateForWord, 0);
    createK(coordinateSystem, startCoordinateForWord, 1);
    createS(coordinateSystem, startCoordinateForWord, 2);
    createH(coordinateSystem, startCoordinateForWord, 3);
    createA(coordinateSystem, startCoordinateForWord, 4);
    createY(coordinateSystem, startCoordinateForWord, 5);
}
// create happy birthday text
void createHappyBirthdayString()
{
    // create coordinate system
    CoordinateSystem coordinateSystem;
    createCoordiNateSystemWithScale(X_RANGE, Y_RANGE, origin_X, origin_Y, SCALE, &coordinateSystem);

    // create start points for 3 strings
    RelativePoint startPointHappy;
    RelativePoint startPointBirthday;
    RelativePoint startPointAkshay;

    // write happy
    startPointHappy.xPosition = -15;
    startPointHappy.yPosition = 3;
    writeHappy(coordinateSystem, startPointHappy);
    // write borthday
    startPointHappy.xPosition = -15;
    startPointHappy.yPosition = -2;
    writeBirthday(coordinateSystem, startPointHappy);

    // write akshay
    startPointHappy.xPosition = -15;
    startPointHappy.yPosition = -7;
    writeAkshay(coordinateSystem, startPointHappy);
}