/*
for simplicity creating color scale would be 0 to 100 in order to use integer value
*/
#ifndef COLOR_SYSTEM
#define COLOR_SYSTEM
#define COLOR_SCALE 100
#define color(x) ((1.0f / COLOR_SCALE) * x)

typedef struct
{
    float red;
    float blue;
    float green;
} Color;

Color RED = {100, 0, 0};
Color BLUE = {0, 100, 0};
Color GREEN = {0, 0, 100};

Color BLACK = {0, 0, 0};
Color GREY = {0.5, 0.5, 0.5};

#endif
